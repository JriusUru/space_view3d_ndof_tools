import bpy
from . import operators


class VIEW3D_PT_ndof_tools_action_panel(bpy.types.Panel):
    bl_label = "Action"
    bl_idname = "VIEW3D_PT_ndof_tools_action_panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = 'NDoF Tools'
    bl_order = 0

    @classmethod
    def poll(cls, context):
        return True

    def draw(self, context):
        layout = self.layout
        if operators.pywinusb_available:
            layout.operator("ndof_tools.telekinesis", icon="ORIENTATION_GIMBAL")
            layout.operator("ndof_tools.grabmove", icon="ORIENTATION_GIMBAL")
        else:
            layout.operator("ndof_tools.install_pywinusb", icon="SCRIPTPLUGINS")


class VIEW3D_PT_ndof_tools_axis_panel(bpy.types.Panel):
    bl_label = "Settings"
    bl_idname = "VIEW3D_PT_ndof_tools_axis_panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = 'NDoF Tools'

    @classmethod
    def poll(cls, context):
        return True

    def draw(self, context):
        layout = self.layout
        prefs = context.preferences.addons[__package__].preferences
        layout.prop(prefs, 'translate_sensitivity')
        layout.prop(prefs, 'rotate_sensitivity')


def register():
    bpy.utils.register_class(VIEW3D_PT_ndof_tools_action_panel)
    bpy.utils.register_class(VIEW3D_PT_ndof_tools_axis_panel)


def unregister():
    bpy.utils.unregister_class(VIEW3D_PT_ndof_tools_axis_panel)
    bpy.utils.unregister_class(VIEW3D_PT_ndof_tools_action_panel)
