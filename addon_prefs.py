import bpy
from bpy.props import *
from . import operators

class AddonPreferences(bpy.types.AddonPreferences):
    """Stores preferences for our plugin which persist across files."""
    bl_idname = __package__

    translate_sensitivity: FloatProperty(name="Translate sensitivity", description="How fast translation should be", options=set(), default=1, min=0, max=10)
    rotate_sensitivity: FloatProperty(name="Rotate sensitivity", description="How fast rotations should be", options=set(), default=0.1, min=0, max=10)

    led_usage: EnumProperty(
        name="LED usage",
        description="Whether to switch device light on or off (will be applied on next startup)",
        items=[
            ("NONE", "Don't change", "Don't switch on or off the device's light"),
            ("ALWAYS_ON", "Always on", "Leave the light on at all times"),
            ("ALWAYS_OFF", "Always off", "Leave the light off at all times"),
            ("CONTEXTUAL", "Contextual", "Light is off by default but turns on when using one an NDoF-specific operator."),
        ],
        default="NONE",
        options=set(),
    )

    def draw(self, context):
        layout = self.layout
        if operators.pywinusb_available:
            layout.prop(self, "translate_sensitivity")
            layout.prop(self, "rotate_sensitivity")
            layout.prop(self, "led_usage")
        else:
            layout.operator("ndof_tools.install_pywinusb", icon="SCRIPTPLUGINS")


def register():
    bpy.utils.register_class(AddonPreferences)


def unregister():
    bpy.utils.unregister_class(AddonPreferences)
