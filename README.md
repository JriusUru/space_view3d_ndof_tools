# NDoF Tools

Various tools to use your NDoF controller as input in Blender. Windows-only for now.
An NDoF controller is some kind of "3D joystick" with 6 degrees of freedom (translation and rotation) like the [SpaceMouse Pro](https://3dconnexion.com/). This allows you to accurately position objects in 3D spaces with no keypress required.


## Installation

Install like any regular Blender add-on.
You will also need to install PyWinUSB, which can be done either in the addon's preferences, or in the tools menu (3D View → N → NDoF Tools). The UI may hang for a few seconds while the plugin is downloaded and installed.
This plugin is Windows-only for now. Suggestions or PR on how to make this cross-platform are welcome.


## Available preferences

- Translation/rotation speed for Grab mode (persists across Blender files).
- LED usage. Allows you to control how the NDoF's LEDs light up (persists across Blender files, but may require restart).


## Available operators

- Telekinesis: controls the currently selected items using the NDoF device.
- Grab and move: locks the selected items to the viewport's camera, allowing you to move both at once.

For both of those operators:
- Available in:
	- Object mode: will move the selected objects (duh).
	- Edit mode (supports mesh, curve, armature in edit and pose mode): will move the selected vertices/edges/faces/points/bones. (For better performance, details like vertex normals will only be recomputed when you confirm the transformation.)
- Options:
	- Left clic: confirm the current transform.
	- Right clic: cancel and revert to the old transform.
	- (Telekinesis only) Scroll wheel: change translation sensitivity. Will be reset upon restarting the operator.
	- (Telekinesis only) View3D > Transform Pivot Point: changes around which point objects/items rotate (mimicks what Blender operators do).
	If in object mode and the selected objects are parented, the plugin will do its best to move independently of the parent and preserve object scaling. Some odd configurations involving non-uniform scaling may still cause issues - this is normal and can't be fixed.
