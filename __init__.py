# TODO: Somehow allow scaling curve bezier control points.
# TODO: Save speed modifier as scene property (mimicks size contraint for proportional editing).
# TODO: Solve grabmove mode jittering (required checking modifications to camera matrix AFTER ndof event was processed)
# TODO: Add locks:
#       - translation xyz, along/locking
#       - rotation xyz, along/locking
#       Save both as scene properties so they persist.
# WIP: (Related) Lock rotation/translation (should add constraint on axis)
# TODO: Provide keybinding for the new operators.
# TODO (related): Blender supports NDoF buttons, may be useful.
# TODO: Add snap mode (snaps object to close surface, mostly downwards). Tricky.
# TODO: Allow creating curve/GP from path ? Ha, I sure ain't doing it myself. Feel free...


bl_info = {
    "name": "NDoF tools",
    "description": "Allows moving object using an NDOF device. Thanks to Julien Roy for inspiration and johnhw for spacenavigator library.",
    "author": "Jrius",
    "version": (1, 0, 0),
    "blender": (3, 1, 2),
    "location": "3D View > N panel",
    "category": "3D View"
}

from . import panels, operators, addon_prefs
import bpy

addon_keymaps = []


def install_pywinusb():
    """Manipulates pip to install the required pywinusb"""
    import subprocess, sys, os
    py_exec = str(sys.executable)
    # ensure pip is installed
    subprocess.call([py_exec, "-m", "ensurepip", "--user"])
    # install pywinusb
    subprocess.call([py_exec, "-m", "pip", "install", f"--target={py_exec[:len('bin/python.exe')]}{os.sep}lib", "pywinusb"])


def register():
    addon_prefs.register()
    operators.register()
    panels.register()
    
    # register the telekinesis key (ctrl-w by default)
    #kc = bpy.context.window_manager.keyconfigs.addon
    #if kc:
        #km = kc.keymaps.new(name="3D View", space_type="VIEW_3D")
        #kmi = km.keymap_items.new("ndof_tools.telekinesis",
            #type="W",
            #value="PRESS",
            #ctrl=True)
        #addon_keymaps.append((km, kmi))
    
    # Switch on/off the light
    if operators.pywinusb_available:
        led_usage = bpy.context.preferences.addons[__package__].preferences.led_usage
        if led_usage == "ALWAYS_ON":
            success = spacenavigator.open()
            if not success:
                raise RuntimeError("Failed to access ndof device")
            spacenavigator.set_led(True)
            spacenavigator.close()
        elif led_usage in {"ALWAYS_OFF", "CONTEXTUAL"}:
            success = spacenavigator.open()
            if not success:
                raise RuntimeError("Failed to access ndof device")
            spacenavigator.set_led(False)
            spacenavigator.close()


def unregister():
    panels.unregister()
    operators.unregister()
    addon_prefs.unregister()
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()
