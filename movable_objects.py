from mathutils import *
import bmesh


class MovableObject:
    """A Blender object which is moved by the NDoF device"""

    def __init__(self, blender_object, reference_matrix):
        self.blender_object = blender_object
        self.original_matrix_world = Matrix(blender_object.matrix_world)
        self.original_scale = Vector(blender_object.scale)
        self.reference_matrix = Matrix(reference_matrix)
        self.inverted_reference_matrix = self.reference_matrix.inverted()

    def apply(self):
        """Applies all the current transforms definitely"""
        # Everything already in place.
        pass

    def reset(self):
        """Cancels all transforms"""
        self.blender_object.matrix_world = self.original_matrix_world

    def transform_matrix_update(self, matrix):
        """Moves the object by the given matrix from its original position"""
        self.blender_object.matrix_world = self.reference_matrix @ matrix @ self.inverted_reference_matrix @ self.original_matrix_world
        self.blender_object.scale = self.original_scale


class MovableObjectVertices:
    """An edit-time mesh whose selected vertices are moved by the NDoF device"""

    def __init__(self, blender_object, reference_matrix=None):
        self.blender_object = blender_object
        self.bmesh = bmesh.from_edit_mesh(blender_object.data)
        # Note: self.bmesh mustn't be freed, because apparently this causes further from_edit_mesh to fail until edit mode is exited.
        # (Printing the bmesh afterwards says it's "dead" so Blender correctly frees it.)
        self.vertices = [(vertex, Vector(vertex.co)) for vertex in self.bmesh.verts if vertex.select]
        if reference_matrix != None:
            self.reference_matrix = Matrix(reference_matrix)
            self.inverted_reference_matrix = self.reference_matrix.inverted()
        else:
            self.reference_matrix = None
            self.inverted_reference_matrix = None

    def get_active_element_position(self):
        active_element = self.bmesh.select_history.active
        if not active_element:
            return None
        elif isinstance(active_element, bmesh.types.BMVert):
            return active_element.co
        elif isinstance(active_element, bmesh.types.BMEdge):
            return (active_element.verts[0].co + active_element.verts[1].co) / 2
        elif isinstance(active_element, bmesh.types.BMFace):
            return active_element.calc_center_median()
        raise RuntimeError("Can't tell type of active element: %s" % active_element)

    def set_reference_matrix(self, reference_matrix):
        self.reference_matrix = Matrix(reference_matrix)
        self.inverted_reference_matrix = self.reference_matrix.inverted()

    def apply(self):
        """Applies all the current transforms definitely"""
        # Let Blender recalculate proper data
        self.bmesh.normal_update()
        bmesh.update_edit_mesh(self.blender_object.data)

    def reset(self):
        """Cancels all transforms"""
        for vertex, original_position in self.vertices:
            vertex.co = original_position
        bmesh.update_edit_mesh(self.blender_object.data)

    def transform_matrix_update(self, matrix):
        """Moves the vertices by the given matrix from their original positions"""
        final_matrix = self.reference_matrix @ matrix @ self.inverted_reference_matrix
        for vertex, original_position in self.vertices:
            vertex.co = final_matrix @ original_position
        # (Note: keep both bools at false for better perf.)
        bmesh.update_edit_mesh(self.blender_object.data, loop_triangles=False, destructive=False)

    def get_locations(self):
        locations = [
            v.co
            for v, _ in self.vertices
        ]
        return locations


class MovableCurvePoints:
    """An edit-time curve whose selected points are moved by the NDoF device"""

    def __init__(self, blender_object, reference_matrix=None):
        self.blender_object = blender_object
        self.points = [
            (
                point,
                Vector(point.co)
            )
            for spline in self.blender_object.data.splines
            for point in spline.points
            if point.select]
        self.bezier_points = [
            (
                point,
                Vector(point.co),
                Vector(point.handle_left),
                Vector(point.handle_right)
            )
            for spline in self.blender_object.data.splines
            for point in spline.bezier_points
            if point.select_control_point or point.select_left_handle or point.select_right_handle]
        if reference_matrix != None:
            self.reference_matrix = Matrix(reference_matrix)
            self.inverted_reference_matrix = self.reference_matrix.inverted()
        else:
            self.reference_matrix = None
            self.inverted_reference_matrix = None

    def get_active_element_position(self):
        raise RuntimeError("I don't know how to find a curve's active point yet, sorry.")

    def set_reference_matrix(self, reference_matrix):
        self.reference_matrix = Matrix(reference_matrix)
        self.inverted_reference_matrix = self.reference_matrix.inverted()

    def apply(self):
        """Applies all the current transforms definitely"""
        # Everything done already.
        pass

    def reset(self):
        """Cancels all transforms"""
        for point, original_position in self.points:
            point.co = original_position
        for point, original_position, original_handle_left, original_handle_right in self.bezier_points:
            point.co = original_position
            point.handle_left = original_handle_left
            point.handle_right = original_handle_right

    def transform_matrix_update(self, matrix):
        """Moves the points by the given matrix from their original positions"""
        final_matrix = self.reference_matrix @ matrix @ self.inverted_reference_matrix
        for point, original_position in self.points:
            point.co = final_matrix @ original_position
        for point, original_position, original_handle_left, original_handle_right in self.bezier_points:
            if point.select_control_point:
                point.co = final_matrix @ original_position
            if point.select_left_handle:
                point.handle_left = final_matrix @ original_handle_left
            if point.select_right_handle:
                point.handle_right = final_matrix @ original_handle_right

    def get_locations(self):
        locations = [point.co for point, _ in self.points]
        locations.extend((
            point.co
            for point, _, _, _ in self.bezier_points
            if point.select_control_point
        ))
        locations.extend((
            point.handle_left
            for point, _, _, _ in self.bezier_points
            if point.select_left_handle
        ))
        locations.extend((
            point.handle_right
            for point, _, _, _ in self.bezier_points
            if point.select_right_handle
        ))
        return locations


class MovableEditBones:
    """An edit-time armature whose selected bones are moved by the NDoF device"""

    def __init__(self, blender_object, reference_matrix=None):
        self.blender_object = blender_object
        self.bones = [
            (
                bone,
                Vector(bone.head),
                Vector(bone.tail)
            )
            for bone in self.blender_object.data.edit_bones
            if bone.select_head or bone.select_tail]
        if reference_matrix != None:
            self.reference_matrix = Matrix(reference_matrix)
            self.inverted_reference_matrix = self.reference_matrix.inverted()
        else:
            self.reference_matrix = None
            self.inverted_reference_matrix = None

    def get_active_element_position(self):
        self.blender_object.data.edit_bones.active

    def set_reference_matrix(self, reference_matrix):
        self.reference_matrix = Matrix(reference_matrix)
        self.inverted_reference_matrix = self.reference_matrix.inverted()

    def apply(self):
        """Applies all the current transforms definitely"""
        # Everything done already.
        pass

    def reset(self):
        """Cancels all transforms"""
        for bone, original_position_head, original_position_tail in self.bones:
            bone.head = original_position_head
            bone.tail = original_position_tail

    def transform_matrix_update(self, matrix):
        """Moves the bones by the given matrix from their original positions"""
        final_matrix = self.reference_matrix @ matrix @ self.inverted_reference_matrix
        for bone, original_position_head, original_position_tail in self.bones:
            if bone.select_head:
                bone.head = final_matrix @ original_position_head
            if bone.select_tail:
                bone.tail = final_matrix @ original_position_tail

    def get_locations(self):
        locations = []
        locations.extend((
            bone.head
            for bone, _, _ in self.bones
            if bone.select_head
        ))
        locations.extend((
            bone.tail
            for bone, _, _ in self.bones
            if bone.select_tail
        ))
        return locations


class MovablePoseBones:
    """A pose-time armature whose selected bones are moved by the NDoF device"""

    def __init__(self, blender_object, reference_matrix=None):
        self.blender_object = blender_object
        self.bones = [
            (
                bone,
                Matrix(bone.matrix)
            )
            for bone in self.blender_object.pose.bones
            if bone.bone.select]
        # TODO - reorder bones by depth in hierarchy.
        if reference_matrix != None:
            self.reference_matrix = Matrix(reference_matrix)
            self.inverted_reference_matrix = self.reference_matrix.inverted()
        else:
            self.reference_matrix = None
            self.inverted_reference_matrix = None

    def get_active_element_position(self):
        active_bone = self.blender_object.data.bones.active
        if not active_bone:
            return None
        active_pose_bone = first((bone for bone in self.blender_object.pose.bones if bone == active_bone))
        return active_pose_bone.matrix.translation

    def set_reference_matrix(self, reference_matrix):
        self.reference_matrix = Matrix(reference_matrix)
        self.inverted_reference_matrix = self.reference_matrix.inverted()

    def apply(self):
        """Applies all the current transforms definitely"""
        # Everything done already.
        pass

    def reset(self):
        """Cancels all transforms"""
        for bone, original_matrix in self.bones:
            bone.matrix = original_matrix

    def transform_matrix_update(self, matrix):
        """Moves the bones by the given matrix from their original positions"""
        final_matrix = self.reference_matrix @ matrix @ self.inverted_reference_matrix
        for bone, original_matrix in self.bones:
            bone.matrix = final_matrix @ original_matrix

    def get_locations(self):
        locations = [
            bone.matrix.translation
            for bone, _ in self.bones
        ]
        return locations
