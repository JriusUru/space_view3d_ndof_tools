import bpy, bgl, blf
from .movable_objects import *
try:
    from . import spacenavigator
    pywinusb_available = True
except ImportError:
    pywinusb_available = False
import time


class TelekinesisOperator(bpy.types.Operator):
    bl_idname = "ndof_tools.telekinesis"
    bl_label = "Telekinesis"
    bl_description = "Move the currently selected objects with the NDOF device"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) != 0 and
            (context.mode in {'OBJECT', 'EDIT_MESH', 'EDIT_CURVE', 'EDIT_SURFACE', 'EDIT_ARMATURE', 'POSE'}))

    def fetch_movable_objects(self, context):
        pivot_option = context.scene.tool_settings.transform_pivot_point
        selected_objects = context.selected_objects

        def get_ref_mtx_for_object(blender_object):
            pivot_center = None
            if pivot_option in {"MEDIAN_POINT",
                                # we COULD compute the BB center... but seriously, why would anyone care ?
                                "BOUNDING_BOX_CENTER"}:
                pivot_center = Vector(selected_objects[0].matrix_world.translation)
                for obj in selected_objects[1:]:
                    pivot_center += obj.matrix_world.translation
                pivot_center /= len(selected_objects)
            elif pivot_option == "CURSOR":
                pivot_center = Vector(context.scene.cursor.location)
            elif pivot_option == "INDIVIDUAL_ORIGINS":
                pivot_center = Vector(blender_object.matrix_world.translation)
            elif pivot_option == "ACTIVE_ELEMENT":
                pivot_center = Vector(context.object.matrix_world.translation)
            else:
                raise RuntimeError("Unsupported pivot option: %s" % pivot_option)
            return Matrix.Translation(pivot_center)

        return [MovableObject(object, get_ref_mtx_for_object(object)) for object in selected_objects]

    def compute_median(positions):
        if len(positions) == 0:
            return None
        median = Vector(positions[0])
        for position in positions[1:]:
            median += position
        median /= len(positions)
        return median

    def compute_bbox_center(positions):
        if len(positions) == 0:
            return None
        position_min = Vector(positions[0])
        position_max = Vector(positions[0])
        for position in positions[1:]:
            x = position.x
            y = position.y
            z = position.z
            if position_min.x > x:
                position_min.x = x
            if position_min.y > y:
                position_min.y = y
            if position_min.z > z:
                position_min.z = z
            if position_max.x < x:
                position_max.x = x
            if position_max.y < y:
                position_max.y = y
            if position_max.z < z:
                position_max.z = z
        return (position_min + position_max) / 2

    def fetch_movable_mesh_vertices(self, context):
        pivot_option = context.scene.tool_settings.transform_pivot_point
        selected_objects = [object for object in context.selected_objects if object.type == 'MESH']

        result = []
        for obj in selected_objects:
            result.append(MovableObjectVertices(obj))
        result = [object for object in result if len(object.vertices) > 0]
        if len(result) == 0:
            # No verts ? I ain't doing anything chief.
            return None
        self.set_reference_matrices(context, result)
        return result

    def fetch_movable_curve_points(self, context):
        pivot_option = context.scene.tool_settings.transform_pivot_point
        if context.mode == 'EDIT_CURVE':
            selected_objects = [object for object in context.selected_objects if object.type == 'CURVE']
        elif context.mode == 'EDIT_SURFACE':
            selected_objects = [object for object in context.selected_objects if object.type == 'SURFACE']

        result = []
        for obj in selected_objects:
            result.append(MovableCurvePoints(obj))
        result = [object for object in result if len(object.points) > 0 or len(object.bezier_points) > 0]
        if len(result) == 0:
            # No points ? I ain't doing anything chief.
            return None
        self.set_reference_matrices(context, result)
        return result
    
    def fetch_movable_armature_edit_bones(self, context):
        selected_objects = [object for object in context.selected_objects if object.type == 'ARMATURE']
        
        result = []
        for obj in selected_objects:
            result.append(MovableEditBones(obj))
        result = [object for object in result if len(object.bones) > 0]
        if len(result) == 0:
            # No bones ? I ain't doing anything chief.
            return None
        self.set_reference_matrices(context, result)
        return result
    
    def fetch_movable_armature_pose_bones(self, context):
        selected_objects = [object for object in context.selected_objects if object.type == 'ARMATURE']
        
        result = []
        for obj in selected_objects:
            result.append(MovablePoseBones(obj))
        result = [object for object in result if len(object.bones) > 0]
        if len(result) == 0:
            # No bones ? I ain't doing anything chief.
            return None
        self.set_reference_matrices(context, result)
        return result
        
    def set_reference_matrices(self, context, objects):
        """Sets reference (base xform) matrices for editable objects (curves, armatures...)."""
        def apply_corrected_reference_matrix(object, location):
            object.set_reference_matrix(object.blender_object.matrix_world.inverted() @ Matrix.Translation(location))

        # For each object, we need a location around which to rotate points...
        # We mimick Blender's own behavior when multi-editing object data (aka "data items").
        
        pivot_option = context.scene.tool_settings.transform_pivot_point
        if pivot_option == "MEDIAN_POINT":
            # Compute median for all selected points across edited data items.
            pivot_center = TelekinesisOperator.compute_median([
                object.blender_object.matrix_world @ point
                for object in objects
                for point in object.get_locations()])
            if pivot_center == None:
                raise RuntimeError("No selected data items ?")
            for object in objects:
                apply_corrected_reference_matrix(object, pivot_center)

        elif pivot_option == "BOUNDING_BOX_CENTER":
            # Compute bbox center for all selected data items across edited curves.
            pivot_center = TelekinesisOperator.compute_bbox_center([
                object.blender_object.matrix_world @ point
                for object in objects
                for point in object.get_locations()])
            if pivot_center == None:
                raise RuntimeError("No selected data items ?")
            for object in objects:
                apply_corrected_reference_matrix(object, pivot_center)

        elif pivot_option == "CURSOR":
            # Pfew, finally something easy.
            pivot_center = context.scene.cursor.location
            for object in objects:
                apply_corrected_reference_matrix(object, pivot_center)

        elif pivot_option == "INDIVIDUAL_ORIGINS":
            # Points turn around each object's median point.
            for object in objects:
                pivot_center = object.blender_object.matrix_world @ TelekinesisOperator.compute_median([
                    point for point in object.get_locations()])
                if pivot_center != None: # only if we have selected vertices on this object
                    apply_corrected_reference_matrix(object, pivot_center)

        elif pivot_option == "ACTIVE_ELEMENT":
            # Data items turn around the currently selected item.
            # TODO: we probably don't know how to get the active element, or on which object it's on.
            active_position = None
            for object in objects:
                active_position = object.get_active_element_position()
                if active_position != None:
                    # got it - just put it in world space and we're good.
                    active_position = object.blender_object.matrix_world @ active_position
                    break
            if active_position == None:
                # Still nothing ? Just default back to median of all curves
                active_position = TelekinesisOperator.compute_median([
                    object.blender_object.matrix_world @ point
                    for object in objects
                    for point in object.get_locations()])
            if active_position == None:
                raise RuntimeError("No selected data items ?")
            for object in objects:
                apply_corrected_reference_matrix(object, active_position)

        else:
            raise RuntimeError("Unsupported pivot option: %s" % pivot_option)

    def modal(self, context, event):
        cur_time = time.time()

        # Can't use icons like MOUSE_RMB for tooltips. We probably need to register those in the keymap ? How would that work ? TODO.
        bpy.context.workspace.status_text_set(text=f"LMB: confirm. RMB: cancel. Translation lock: {self.lock_translation}. Rotation lock: {self.lock_rotation}")
        bpy.context.area.header_text_set("Sensitivity: {0:.2f}".format(self.translate_sensitivity))

        if event.type == 'NDOF_MOTION':
            # NDoF device is held/twisted in a direction.
            state = spacenavigator.read()

            time_difference = cur_time - self.last_time
            time_sensitivity = min(time_difference, 0.05) * 60
            distance_sensitivity = bpy.context.space_data.region_3d.view_distance / 20

            # (We could use another reference matrix here if needed...)
            # (But would this ever make sense, since view space transform is the most intuitive for NDoF devices ?)
            ref_mtx = bpy.context.space_data.region_3d.view_matrix
            ref_quat = ref_mtx.to_quaternion()

            ndof_translation = Vector((state.x, state.z, -state.y)) * self.translate_sensitivity * time_sensitivity * distance_sensitivity
            ndof_rotation = Quaternion(Vector((-state.pitch, -state.yaw, -state.roll)) * self.rotate_sensitivity * time_sensitivity)

            translation = Matrix.Translation(self.movement_matrix.translation)
            translation @= Matrix.Translation(ndof_translation @ ref_mtx.to_3x3())
            rotation = self.movement_matrix.to_quaternion()
            rotation = (ref_quat.inverted() @ ndof_rotation @ ref_quat) @ rotation

            if self.lock_translation and self.lock_rotation:
                # Should never happen
                raise RuntimeError("Unexpected: locking both translation and rotation.")
            elif self.lock_translation:
                self.movement_matrix = rotation.to_matrix().to_4x4()
            elif self.lock_rotation:
                self.movement_matrix = translation
            else:
                # both free
                self.movement_matrix = translation @ rotation.to_matrix().to_4x4()

            for object in self.objects:
                object.transform_matrix_update(self.movement_matrix)

        elif event.type == 'LEFTMOUSE':
            self.apply()
            return {'FINISHED'}

        elif event.type in {'RIGHTMOUSE', 'ESC'}:
            self.abort();
            return {'CANCELLED'}

        elif event.type in {'WHEELUPMOUSE', 'WHEELDOWNMOUSE'}:
            # Mouse wheel increases or decreases translation sensitivity (this once only, not saved into prefs).
            # (We don't modify rotational sensitivity, since this isn't dependant on object size/positioning.)
            if event.type == 'WHEELUPMOUSE':
                self.translate_sensitivity = min(10, self.translate_sensitivity * 1.1)
            else:
                self.translate_sensitivity = max(0.1, self.translate_sensitivity / 1.1)
        
        elif event.type == "G" and event.value == "PRESS":
            self.lock_translation = not self.lock_translation
            if self.lock_translation and self.lock_rotation:
                self.lock_rotation = False
        elif event.type == "R" and event.value == "PRESS":
            self.lock_rotation = not self.lock_rotation
            if self.lock_translation and self.lock_rotation:
                self.lock_translation = False

        self.last_time = cur_time

        if event.type in {'MOUSEMOVE', 'MIDDLEMOUSE'}:
            # Let the user orbit with the mouse... May be useful.
            return {'PASS_THROUGH'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        if len(context.selected_objects):
            prefs = context.preferences.addons[__package__].preferences
            success = spacenavigator.open()
            if not success:
                raise RuntimeError("Failed to access ndof device")
            led_usage = prefs.led_usage
            if led_usage == "CONTEXTUAL":
                spacenavigator.set_led(True)

            if context.mode == 'OBJECT':
                self.objects = self.fetch_movable_objects(context)
            elif context.mode == 'EDIT_MESH':
                self.objects = self.fetch_movable_mesh_vertices(context)
            elif context.mode in {'EDIT_CURVE', 'EDIT_SURFACE'}:
                self.objects = self.fetch_movable_curve_points(context)
            elif context.mode == 'EDIT_ARMATURE':
                self.objects = self.fetch_movable_armature_edit_bones(context)
            elif context.mode == 'POSE':
                self.objects = self.fetch_movable_armature_pose_bones(context)
            else:
                raise RuntimeError("Unsupported context mode")
            if self.objects == None or len(self.objects) == 0:
                # You have GOT to be kidding me.
                self.report({'ERROR'}, "No movable elements selected")
                return {'CANCELLED'}
            self.movement_matrix = Matrix.Identity(4)
            self.translate_sensitivity = prefs.translate_sensitivity
            self.rotate_sensitivity = prefs.rotate_sensitivity
            self.last_time = time.time()
            self.lock_translation = False
            self.lock_rotation = False
            context.window_manager.modal_handler_add(self)
            return {'RUNNING_MODAL'}
        else:
            self.report({'WARNING'}, "No selected object")
            return {'CANCELLED'}

    def apply(self):
        """Apply transforms and exit."""
        for object in self.objects:
            object.apply()
        bpy.context.area.header_text_set(None)
        bpy.context.workspace.status_text_set(text=None)
        prefs = bpy.context.preferences.addons[__package__].preferences
        led_usage = prefs.led_usage
        if led_usage == "CONTEXTUAL":
            spacenavigator.set_led(False)
        spacenavigator.close()

    def abort(self):
        """Cancel all movement and restore transforms."""
        for object in self.objects:
            object.reset()
        bpy.context.area.header_text_set(None)
        bpy.context.workspace.status_text_set(text=None)
        prefs = bpy.context.preferences.addons[__package__].preferences
        led_usage = prefs.led_usage
        if led_usage == "CONTEXTUAL":
            spacenavigator.set_led(False)
        spacenavigator.close()


class GrabMoveOperator(bpy.types.Operator):
    bl_idname = "ndof_tools.grabmove"
    bl_label = "Grab and move"
    bl_description = "Move the selected objects with the camera"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return (len(context.selected_objects) != 0 and
            (context.mode in {'OBJECT', 'EDIT_MESH', 'EDIT_CURVE', 'EDIT_SURFACE', 'EDIT_ARMATURE', 'POSE'}))

    def modal(self, context, event):
        cur_time = time.time()

        #bpy.context.workspace.status_text_set(text="Hello World") # should be used for key tooltips...
        #bpy.context.area.header_text_set("?")

        if event.type == 'NDOF_MOTION':
            # NDoF device is held/twisted in a direction.
            state = spacenavigator.read()

            matrix = self.original_matrix_view @ bpy.context.space_data.region_3d.view_matrix.inverted()
            for object in self.objects:
                object.transform_matrix_update(matrix)

            return {'PASS_THROUGH'}

        elif event.type == 'LEFTMOUSE':
            self.apply()
            return {'FINISHED'}

        elif event.type in {'RIGHTMOUSE', 'ESC'}:
            self.abort()
            return {'CANCELLED'}

        if event.type in {'MOUSEMOVE', 'MIDDLEMOUSE', 'WHEELUPMOUSE', 'WHEELDOWNMOUSE'}:
            # Let the user orbit with the mouse... May be useful.
            # Also let the user scroll around, since that's used to (indirectly) control translation speed of the camera.
            return {'PASS_THROUGH'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        if len(context.selected_objects):
            prefs = context.preferences.addons[__package__].preferences
            success = spacenavigator.open()
            if not success:
                raise RuntimeError("Failed to access ndof device")
            led_usage = prefs.led_usage
            if led_usage == "CONTEXTUAL":
                spacenavigator.set_led(True)

            if context.mode == 'OBJECT':
                self.objects = [MovableObject(object, bpy.context.space_data.region_3d.view_matrix.inverted()) for object in context.selected_objects]
            elif context.mode == 'EDIT_MESH':
                self.objects = [
                    MovableObjectVertices(object, object.matrix_world.inverted() @ bpy.context.space_data.region_3d.view_matrix.inverted())
                    for object in context.selected_objects
                    if object.type == 'MESH']
                self.objects = [object for object in self.objects if len(object.vertices) > 0]
            elif context.mode in {'EDIT_CURVE', 'EDIT_SURFACE'}:
                data_type = 'CURVE' if context.mode == 'EDIT_CURVE' else 'SURFACE'
                self.objects = [
                    MovableCurvePoints(object, object.matrix_world.inverted() @ bpy.context.space_data.region_3d.view_matrix.inverted())
                    for object in context.selected_objects
                    if object.type == data_type]
            elif context.mode == 'EDIT_ARMATURE':
                self.objects = [
                    MovableEditBones(object, object.matrix_world.inverted() @ bpy.context.space_data.region_3d.view_matrix.inverted())
                    for object in context.selected_objects
                    if object.type == 'ARMATURE']
            elif context.mode == 'POSE':
                self.objects = [
                    MovablePoseBones(object, object.matrix_world.inverted() @ bpy.context.space_data.region_3d.view_matrix.inverted())
                    for object in context.selected_objects
                    if object.type == 'ARMATURE']
            else:
                raise RuntimeError("Unsupported context mode")
            if self.objects == None or len(self.objects) == 0:
                # You have GOT to be kidding me.
                self.report({'ERROR'}, "No movable elements selected")
                return {'CANCELLED'}
            self.original_matrix_view = Matrix(bpy.context.space_data.region_3d.view_matrix)
            context.window_manager.modal_handler_add(self)
            return {'RUNNING_MODAL'}
        else:
            self.report({'WARNING'}, "No selected object")
            return {'CANCELLED'}

    def apply(self):
        """Apply transforms and exit."""
        for object in self.objects:
            object.apply()
        bpy.context.area.header_text_set(None)
        prefs = bpy.context.preferences.addons[__package__].preferences
        led_usage = prefs.led_usage
        if led_usage == "CONTEXTUAL":
            spacenavigator.set_led(False)
        spacenavigator.close()

    def abort(self):
        """Cancel all movement and restore transforms. Also moves the viewport camera back to the starting position."""
        for object in self.objects:
            object.reset()
        bpy.context.space_data.region_3d.view_matrix = self.original_matrix_view
        bpy.context.area.header_text_set(None)
        prefs = bpy.context.preferences.addons[__package__].preferences
        led_usage = prefs.led_usage
        if led_usage == "CONTEXTUAL":
            spacenavigator.set_led(False)
        spacenavigator.close()


class InstallPywinusbOperator(bpy.types.Operator):
    bl_idname = "ndof_tools.install_pywinusb"
    bl_label = "Install PyWinUSB"
    bl_description = "Required for NDOF Tools to function"

    def install_pywinusb(self):
        """Manipulates pip to install the required pywinusb"""
        global pywinusb_available
        import subprocess, sys, os
        py_exec = str(sys.executable)
        # ensure pip is installed
        subprocess.call([py_exec, "-m", "ensurepip", "--user"])
        # install pywinusb
        subprocess.call([py_exec, "-m", "pip", "install", f"--target={py_exec[:-len('bin/python.exe')]}{os.sep}lib", "pywinusb"])
        from . import spacenavigator
        pywinusb_available = True
    
    def invoke(self, context, event):
        global pywinusb_available
        if pywinusb_available:
            self.report({'WARNING'}, "PyWinUSB already available")
        else:
            self.install_pywinusb()
            if pywinusb_available:
                bpy.utils.register_class(TelekinesisOperator)
                bpy.utils.register_class(GrabMoveOperator)
        return {'FINISHED'}


def register():
    global pywinusb_available
    bpy.utils.register_class(InstallPywinusbOperator)
    if pywinusb_available:
        bpy.utils.register_class(TelekinesisOperator)
        bpy.utils.register_class(GrabMoveOperator)


def unregister():
    bpy.utils.unregister_class(GrabMoveOperator)
    bpy.utils.unregister_class(TelekinesisOperator)
    bpy.utils.unregister_class(InstallPywinusbOperator)
